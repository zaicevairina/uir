
import requests
import json
import csv
import time

# group_name = ['spb', 'peterburg_dtp','novosti_spb', 'piteronline24','nevablog','pitertv','bspb','topspb_tv']
group_name = ['bspb','novosti_spb']
def take_1000_posts(domain):
    token = 'e7a79876e7a79876e7a79876e9e7ce3561ee7a7e7a79876bb0457d3e507797f75821138'
    version = 5.92
    count = 100
    offset = 0
    all_posts = []
    x = 100
    while (x==100):
        response = requests.get('https://api.vk.com/method/wall.get',
                                params={
                                    'access_token': token,
                                    'v': version,
                                    'domain': domain,
                                    'count': count,
                                    'offset':offset
                                }
                                )
        data = response.json()['response']['items']
        offset += 100
        all_posts.extend(data)
        time.sleep(0.5)
        x=len(data)
    return all_posts


def file_writer(data,domain):
    with open('file.csv', 'a', encoding='utf8') as file:
        a_pen = csv.writer(file)
        for post in data:
            a_pen.writerow((domain,post['text'],post['date']))

for gr_name in group_name:
    all_posts=take_1000_posts(gr_name)
    file_writer(all_posts,gr_name)
print(1)
